package com.example.shobbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShobbeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShobbeApplication.class, args);
    }

}
