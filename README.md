## Project structure

* /api : Contain REST API classes (Spring RestControllers)
  * exception: Define API exception
  * security: Configure api security (using Spring security)
* /core : Contain core classes (domain)
* /application: Provide services
* /infrastructure: Implement external particular services or repositories, ...

## Java convention
[Convention](https://google.github.io/styleguide/javaguide.html#s5-naming)

## Run spring boot app

Run by mvn

`./mvnw spring-boot:run`

Or package a jar file

`./mvnw clean package`

`java -jar target/social-blogging-site-0.0.1-SNAPSHOT.jar`

